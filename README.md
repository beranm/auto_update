# `auto_update` app

This is an custom app for needs of Lukapo to update our servers. It should mitigate majority of our error when we do `apt update && apt upgrade`. It does not have abilities of golden cow.

## Basic workflow

This part of text should describe how the upgrade of the server is done and what step are taken to finnish the task:

1) update mirrors by `apt update`
  - in case of fail, just stop there, inform the admin about failed node and give up

2) get list of packages to be updated

3) find the current versions installed and put them into the apt cache on the server
  - you also have to collect all the dependencies of the packages, cause in case of fail, you should roll them back as well

4) get the list of the running services
  - get the output of `journalctl -u`

5) do the upgrade of all the packages
  - this is tricky as hell, I guess you can do the hell by passing `DEBIAN_FRONTEND=noninteractive` and hope for the best tho

6) in case of reboot-required just reboot the machine if it is approved

7) check the services if they run as they used to run before the upgrade
  - in case of service fail, print the lines in red
  - in case service is running just print it in green and that's it


### Hooks

For some reasons, some servers needs to do jobs before reboot and afer reboot. E.g.: remove server from load balancer and after reboot add server to load balancer; stop all supervisor tasks gently, reboot and start them again; ... Therefore there are hooks implemented. Currently there are two:

#### `<hostname>_reboot.sh.t2`

This script runs before reboot of the target machine.

#### `<hostname>_after_reboot.sh.t2`

This script runs after reboot of the target machine.

### Defined variables in Jinja2 hooks templates

There are currently few variables passed into hook files:

  - hostname -- hostname used for ssh connection
  - user -- user used for ssh connection
  - port -- port of ssh connection used to connect to the server

This variables can be referenced in hook files as `{{ hostname }}`.

## What we have used

In this section we went through some of the options which could have been used as languages/plugins to finnish the task. This could be unnecessary to have here for all the read but on the other hand it could help understand the frustration of automatisation in wild world of the devops. Therefore we described with a few lines why we picked Fabric instead of Ansible.

### Ansible

Yes! But! There are some constraints which could be overcome by new module, custom scripts and so on. The approach should one day reach one nice ansible role. But there is a long way. The main problem is that if machine would reload, ssh session would get lost. I know there is a way how to reconnect after ssh fail, but it didn't look that clear. Why tight your hands with Ansible if you can create everything form the thin air.


### Paramico

Too low level, would need to do too much for so little.

### Spur

The argument was made in this [article](https://dtucker.co.uk/hack/ssh-for-python-in-search-of-api-perfection.html) already.

### Fabric

Seems to be the thing to use tho. After few lines it come clear there are plenty limitation. E.g. horrible bug with username change even if you explicitely define it. This was savige!

## HOWTO use

First of all, define yourselve inventory. There is an example:
```
- group: dummies
  type: parallel
  hosts:
    - 10.0.1.4: allow_to_reboot
    - 10.0.1.1
    - 10.0.1.26: allow_to_reboot
    - 10.0.1.47
    - 10.0.7.231
    - 10.0.1.23
- group: network_servers
  type: sequential
  hosts:
    - 10.0.1.5
    - 10.0.0.5
- group: workers
  type: sequential
  hosts:
    - 10.0.1.47
    - 10.0.7.242
    - 10.0.7.231
- group: webservers
  type: sequential
  hosts:
    - 10.0.0.1
```

As you can see, there are plenty of IP adresses which could be hostnames. This doesn't matter, it is the same as ssh connection string: e.g.: `user@hostname:port`.

Then you need to follow help:

```
nemo@nemonb:~/auto_update (master)$ python ./main.py --help
usage: main.py [-h] [-i INVENTORY] [-p PORT] [-K] [-c] [-u USER]

Auto updater

optional arguments:
  -h, --help            show this help message and exit
  -i INVENTORY, --inventory INVENTORY
                        Inventory of servers
  -p PORT, --port PORT  Servers' port
  -K, --sudo-pass       Sudo password
  -c, --check           Cold run
  -u USER, --user USER  Which user should be used

```

## Example


`python main.py -i inventories/example.yml -p 9022 -u lukapo -K -c`

 - `-K` asks for sudo password, if your server does not require sudo password, you can leave that option
 - `-c` runs only the tasks which do not have destruction nature
 - `-u` user used for whole inventory
 - `-p` port of ssh
 - `-i` path to inventory



