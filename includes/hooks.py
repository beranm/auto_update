from jinja2 import Environment
import logging


class hooks:
    def __init__(self, _command_fac):
        self._command_fac = _command_fac

    def call_hook(self, hook_name):
        try:
            with open(
                './hook_files/' + self._command_fac.hostname +
                '_' + hook_name + '.sh.t2',
                'r'
            ) as temp_file:
                logging.info(
                    'Opening ./hook_files/' + self._command_fac.hostname +
                    '_' + hook_name + '.sh.t2'
                )
                formatted_temp_file = Environment().from_string(
                    temp_file.read()
                ).render(
                    hostname=self._command_fac.hostname,
                    user=self._command_fac.user,
                    port=self._command_fac.port
                )
                self._command_fac.sudo(
                    formatted_temp_file
                )
        except FileNotFoundError:
            logging.info(
                'Opening ./hook_files/' + self._command_fac.hostname +
                '_' + hook_name + '.sh.t2 failed, continue'
            )

    def __getattr__(self, attr):
        return self.call_hook(attr)
