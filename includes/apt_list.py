from .apt_list_item import apt_list_item


class apt_list():
    def __init__(self):
        self.packages = []

    def parse_packages(self, apt_output):
        # print(apt_output.stdout)
        results = []
        lines = [
            i.split('/') for i in apt_output.stdout.split('\n')
        ]
        for line in lines:
            if len(line) > 1:
                results.append(line)
        if len(results) > 1:
            del results[0:4]
            # del results[-1]
        for i in results:
            # print(i)
            self.packages = [
                apt_list_item(i[0], i[1].split()[1]) for i in results
            ]
        return results

    def __str__(self):
        ret = ""
        for i in self.packages:
            ret = ret + "\n" + str(i)
        return ret
