import invoke
# from .command_fac import command_fac
from .systemctl_servicer import systemctl_servicer
from .apt_list import apt_list
from .madison_package import madison_package
from .etc import etc
from .command_failed_exception import command_failed_exception
import time
from .wait_for_ssh import wait_for_ssh
from .hooks import hooks


class updater:

    def __init__(self, _command_fac):
        self._command_fac = _command_fac
        self._systemctl_servicer = systemctl_servicer()
        self._list_of_packages_for_update = apt_list()
        self._hooks = hooks(_command_fac)

    def checkout_list_of_packages_for_update(self):
        try:
            result = self._command_fac.sudo("apt list --upgradable | cat")
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "List of packages to upgrade failed!"
            )
            return
        return result

    def checkout_packages(self):
        result = self.checkout_list_of_packages_for_update()
        self._list_of_packages_for_update.parse_packages(result)
        return self._list_of_packages_for_update

    def update_repo(self):
        try:
            result = self._command_fac.sudo("apt-get update")
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "apt-get update failed!"
            )
            return
        return result

    def checkout_systemctl_units(self):
        try:
            result = self._command_fac.run(
                "systemctl list-units --type=service --state=running", False)
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "systemctl list-units command failed!"
            )
            return
        return result

    def checkout_services(self):
        result = self.checkout_systemctl_units()
        self._systemctl_servicer.parse_units(result)
        # logging.info(self._systemctl_servicer)
        return self._systemctl_servicer

    def update_server(self):
        try:
            self._command_fac.sudo(
                "DEBIAN_FRONTEND=noninteractive " +
                "apt-get -o Dpkg::Options::=\"--force-confdef\" " +
                "-o Dpkg::Options::=\"--force-confold\" upgrade -y")
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "apt-get upgrade failed!"
            )

    def save_etc(self):
        # this could be much simplier if there is etckeeper
        #  could be nice feature in future (FiF)
        _etc = etc(self._command_fac)
        _etc.archive()

    def compare_services_state(self):
        result = self.checkout_systemctl_units()
        self._new_systemctl_servicer = systemctl_servicer()
        self._new_systemctl_servicer.parse_units(result)
        return self._systemctl_servicer.compare(
            self._new_systemctl_servicer
        )

    def download_old_packages_to_cache(self):
        self.checkout_packages()
        for package in self._list_of_packages_for_update.packages:
            _madison_package = madison_package(self._command_fac, package)
            _madison_package.download_current_version_to_cache()

    def test_if_reboot_is_needed(self):
        try:
            result = self._command_fac.run(
                "[[ -f /run/reboot-required ]] && echo yep || echo nope"
            )
            if result.stdout.strip() == "yep":
                return True
            else:
                return False
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "[[ -f /run/reboot-required ]] && echo yep || echo nope failed"
            )
            return
        return result

    def reboot_if_needed(self):
        try:
            result = self._command_fac.run(
                "[[ -f /run/reboot-required ]] && echo yep || echo nope"
            )
            if result.stdout == "yep":
                self._hooks.reboot
                result = self._command_fac.sudo(
                    "{ sleep 10 && sudo shutdown -r now } &"
                )
                self._command_fac.close()
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "shutdown -r +1 failed"
            )
            return
        return result

    def wait_a_minute_for_machine(self):
        i = 0
        while True:
            if i == 1:
                return False
            time.sleep(60)
            if wait_for_ssh(
                self._command_fac.hostname, self._command_fac.port
            ):
                break
            i += 1
        return True
