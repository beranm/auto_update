import socket


def wait_for_ssh(hostname, port):
    s = socket.socket()
    try:
        s.connect((hostname, port))
    except socket.gaierror:
        return False
    return True
