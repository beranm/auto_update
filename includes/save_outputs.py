import datetime
import os
import logging

class save_outputs:
    def __init__(self, hostname):
        dtime = datetime.datetime.now()
        self.dtime = dtime.strftime("%y%m%d")
        self.hostname = hostname
        self.create_log_dir()

    def create_log_dir(self):
        # define the access rights
        access_rights = 0o755
        self.path = "./updater_logs/" + self.hostname
        # try:
        try:
            os.mkdir("./updater_logs/", access_rights)
        except FileExistsError:
            logging.debug("Folder %s created" % "./updater_logs/")
        try:
            os.mkdir(self.path, access_rights)
        except FileExistsError:
            logging.debug("Folder %s already in place" % self.path)
        # except OSError:
        #     print ("Folder %s already in place" % self.path)
        # else:
        #     print ("Folder %s created" % self.path)

    def save_output(self, text_input):
        with open(self.path + "/log_" + self.dtime, 'a') as out_file:
            out_file.write("=============================================\n")
            out_file.write(text_input)
