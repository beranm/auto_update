import invoke
import datetime
# from .apt_list_item import apt_list_item
from .command_failed_exception import command_failed_exception


class madison_package:

    def __init__(self, _command_fac, package):
        self._command_fac = _command_fac
        self.package = package
        self.versions = []
        self.current_version = ""
        self.to_download_version = ""

    def checkout_current_version(self):
        try:
            result = self._command_fac.run(
                "apt-cache show " + self.package.name, False)
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "apt-cache show " + self.package.name + " failed badly!"
            )
        return result

    def parse_current_version(self, result):
        for line in result.stdout.split('\n'):
            line_pipe_seperated = line.split()
            if len(line_pipe_seperated) > 1:
                if line_pipe_seperated[0] == 'Version:':
                    self.version = line_pipe_seperated[1]
                    break
        return self.version

    def checkout_package_madison(self):
        try:
            result = self._command_fac.run(
                "apt-cache madison " + self.package.name, False)
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "Madison for package " + self.package.name + " failed badly!"
            )
        # logging.error(result)
        return result

    def download_package(self, package_version):
        x = datetime.datetime.now()
        dtime = x.strftime("%y%m%d")
        try:
            result = self._command_fac.run(
                "[[ ! -d auto_updater/packages_cache_" + dtime + " ]]" +
                " && mkdir -p auto_updater/packages_cache_" + dtime + ";" +
                "cd auto_updater/packages_cache_" + dtime + ";" +
                "chown _apt .;" +
                "apt-get download -y " +
                self.package.name + "=" +
                package_version,
                False)
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "Download of package " +
                self.package.name +
                "=" +
                package_version +
                " failed badly!"
            )
        return result

    def parse_and_sort_version_of_package(self, result):
        for line in result.stdout.split('\n'):
            line_pipe_seperated = line.split('|')
            if len(line_pipe_seperated) > 1:
                self.versions.append(
                    line_pipe_seperated[1].strip()
                )
        self.versions.sort()
        return self.versions

    def download_current_version_to_cache(self):
        result = self.checkout_current_version()
        self.parse_current_version(result)
        result = self.checkout_package_madison()
        self.parse_and_sort_version_of_package(result)
        self.to_download_version = self.version
        self.download_package(self.to_download_version)
