from fabric import Connection, Config
from .save_outputs import save_outputs
from .command_failed_exception import command_failed_exception
import logging


class command_fac:

    def __init__(self, hostname, user, port=22, sudo_pass=""):
        self.hostname = hostname
        self.user = user
        self.port = int(port)
        port = int(port)
        self.sudo_pass = sudo_pass
        self._save_outputs = save_outputs(hostname)
        logging.info(
            "Connection string " + user + "@" + hostname + ":" + str(port)
        )
        if sudo_pass != "":
            self.config = Config(
                overrides={
                    'sudo': {
                        'password': sudo_pass
                    }
                }
            )
            self.connection = Connection(
                hostname,
                user=user,
                port=port,
                config=self.config,
                connect_kwargs={
                    "password": sudo_pass
                }
            )
        else:
            self.connection = Connection(
                hostname,
                user=user,
                port=port
            )

    def sudo(self, command):
        try:
            result = self.connection.sudo(
                command,
                pty=True,
                hide=True
            )
        except AttributeError:
            raise command_failed_exception(
                "SSH failed, check credentials"
            )
        except KeyError:
            raise command_failed_exception(
                "SSH failed, check connection"
            )
        self._save_outputs.save_output(result.stdout)
        return result

    def close(self):
        self.connection.close()

    def reconnect(self):
        self = command_fac(self.hostname, self.user, self.port, self.sudo_pass)

    def run(self, command, pty=True):
        try:
            result = self.connection.run(
                command,
                pty=pty,
                hide=True
            )
        except AttributeError:
            raise command_failed_exception(
                "SSH failed, check credentials"
            )
        except KeyError:
            raise command_failed_exception(
                "SSH failed, check connection"
            )
        self._save_outputs.save_output(result.stdout)
        return result
