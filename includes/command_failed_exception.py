
class command_failed_exception(RuntimeError):
    def __init__(self, error_string):
        self.args += (error_string, )

    def __str__(self):
        return str(self.args[0])
