import logging
from .host import host


class group:
    def __init__(self, inventory_group):
        self.hosts = []
        self.type = inventory_group['type'] \
            if 'type' in inventory_group.keys() else 'sequential'
        self.group = inventory_group['group'] \
            if 'group' in inventory_group.keys() else 'some_random_group'

        if 'hosts' not in inventory_group.keys():
            logging.error("Directive hosts missing in inventory")
            raise ValueError("Your inventory misses hosts in groups")
        self.parse_hosts(inventory_group['hosts'])

    def parse_hosts(self, hosts):
        for inventory_host in hosts:
            self.hosts.append(
                host(
                    inventory_host
                )
            )

    def __str__(self):
        tmp = ""
        tmp += self.group + " updating in " + self.type + ":\n"
        for _host in self.hosts:
            tmp += str(_host) + "\n"
        return tmp
