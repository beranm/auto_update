from includes.updater import updater
from colorama import Fore, Style
from .command_failed_exception import command_failed_exception
import paramiko
from .hooks import hooks


def update(_command_fac, allowed_to_reboot, check=False):
    _updater = updater(_command_fac)
    _hooks = hooks(_command_fac)

    print(
        Fore.GREEN +
        _command_fac.hostname + ": " +
        "checking out the services which are running:" +
        Style.RESET_ALL
    )

    try:
        _updater.checkout_services()
    except command_failed_exception as e:
        print(
            Fore.RED +
            _command_fac.hostname + ": " +
            "CHECK OF RUNNING SERVICES FAILED: " +
            str(e) +
            Style.RESET_ALL
        )
        return

    # to be implemented
    # _updater.collect_few_logs_of_services()

    print(
        Fore.GREEN +
        _command_fac.hostname + ": " +
        "Saving etc folder" +
        Style.RESET_ALL
    )

    try:
        _updater.save_etc()
    except command_failed_exception as e:
        print(
            Fore.RED +
            _command_fac.hostname + ": " +
            "SAVING OF ETC FAILED: " +
            str(e) +
            Style.RESET_ALL
        )
        return

    print(
        Fore.GREEN +
        _command_fac.hostname + ": " +
        "Saving current version of packages into " +
        "packages_cache_<date> archive folder" +
        Style.RESET_ALL
    )

    try:
        _updater.download_old_packages_to_cache()
    except command_failed_exception as e:
        print(
            Fore.RED +
            _command_fac.hostname + ": " +
            "DOWNLOADING PACKAGES TO CACHE FAILED: " +
            str(e) +
            Style.RESET_ALL
        )
        return

    if check is False:
        print(
            Fore.YELLOW +
            _command_fac.hostname + ": " +
            "Updating the server" +
            Style.RESET_ALL
        )
        try:
            _updater.update_server()
        except command_failed_exception as e:
            print(
                Fore.RED +
                _command_fac.hostname + ": " +
                "UPGRADE OF SERVER FAILED: " +
                str(e) +
                Style.RESET_ALL
            )
            return

    # to be implemnted
    # _udpater.compare_logs_with_new_ones()

    print(
        Fore.GREEN +
        _command_fac.hostname + ": " +
        "Comparing the services" +
        Style.RESET_ALL
    )

    differences = _updater.compare_services_state()

    if len(differences.services) > 0:
        print(
            Fore.RED +
            _command_fac.hostname + ": " +
            "There are some services which failed after update. " +
            "Previous state was following:"
        )
        print(differences)
        print(Style.RESET_ALL)

    if check is False:
        if allowed_to_reboot:
            try:
                if _updater.test_if_reboot_is_needed():
                    print(
                        Fore.RED +
                        _command_fac.hostname + ": " +
                        "Rebooting the server" +
                        Style.RESET_ALL
                    )
                else:
                    print(
                        Fore.RED +
                        _command_fac.hostname + ": " +
                        "Reboot of the server not needed" +
                        Style.RESET_ALL
                    )
                    allowed_to_reboot = False
            except command_failed_exception as e:
                print(
                    Fore.RED +
                    _command_fac.hostname + ": " +
                    "Test if reboot is needed failed: " +
                    str(e) +
                    Style.RESET_ALL
                )
                return

            if allowed_to_reboot:
                try:
                    _hooks.reboot
                    _updater.reboot_if_needed()
                except command_failed_exception as e:
                    print(
                        Fore.RED +
                        _command_fac.hostname + ": " +
                        "REBOOT OF SERVER FAILED: " +
                        str(e) +
                        Style.RESET_ALL
                    )
                    return
                print(
                    Fore.YELLOW +
                    _command_fac.hostname + ": " +
                    "WAITING FOR REBOOT TO FINNISH" +
                    Style.RESET_ALL
                )
                while True:
                    i = 0
                    if _updater.wait_a_minute_for_machine():
                        try:
                            _command_fac.reconnect()
                            break
                        except paramiko.ssh_exception.ProxyCommandFailure:
                            print(
                                Fore.YELLOW +
                                _command_fac.hostname + ": " +
                                "MACHINE NOT READY AFTER " + str(i + 1) +
                                " MINUTE" +
                                Style.RESET_ALL
                            )
                    else:
                        print(
                            Fore.YELLOW +
                            _command_fac.hostname + ": " +
                            "MACHINE NOT READY AFTER " + str(i + 1) +
                            " MINUTE" +
                            Style.RESET_ALL
                        )
                    if i == 10:
                        print(
                            Fore.RED +
                            _command_fac.hostname + ": " +
                            "REBOOT AFTER " + str(i + 1) +
                            " MINUTE, GIVING UP!" +
                            Style.RESET_ALL
                        )
                        return
                    i += 1

            _hooks.after_reboot
            differences = _updater.compare_services_state()
            print(
                Fore.GREEN +
                _command_fac.hostname + ": " +
                "Comparing the services after reboot" +
                Style.RESET_ALL
            )
            if len(differences.services) > 0:
                print(
                    Fore.RED +
                    _command_fac.hostname + ": " +
                    "There are some services which failed after reboot. " +
                    "Previous state was following:"
                )
                print(differences)
                print(Style.RESET_ALL)
            print(
                Fore.GREEN +
                _command_fac.hostname + ": " +
                "Everything done!"
            )
            print(differences)
            print(Style.RESET_ALL)
