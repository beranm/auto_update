
class host:
    def __init__(self, inventory_host):
        if isinstance(inventory_host, dict):
            name_of_host = list(inventory_host.keys())[0]
            if 'allow_to_reboot' in inventory_host[name_of_host]:
                self.allowed_to_reboot = True
            else:
                self.allowed_to_reboot = False
            self.hostname = name_of_host
        else:
            self.hostname = inventory_host
            self.allowed_to_reboot = False

    def add_command_fac(self, command_fac):
        self.command_fac = command_fac

    def __str__(self):
        return \
            self.hostname + \
            " allow to reboot " + \
            str(self.allowed_to_reboot)
