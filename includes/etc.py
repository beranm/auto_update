import datetime
import invoke
from .command_failed_exception import command_failed_exception


class etc:
    def __init__(self, _command_fac):
        self._command_fac = _command_fac

    def archive(self):
        x = datetime.datetime.now()
        dtime = x.strftime("%y%m%d_%H%M%S")
        archive_name = "etc_backup_auto_update_" + dtime + ".tar.gz"
        try:
            result = self._command_fac.run(
                "[[ ! -d auto_updater ]] && mkdir auto_updater || echo Directory created"
            )
            result = self._command_fac.sudo(
                "tar cvfz auto_updater/" + archive_name + " /etc"
            )
        except invoke.exceptions.UnexpectedExit:
            raise command_failed_exception(
                "Etc backup have failed!"
            )
        return result
