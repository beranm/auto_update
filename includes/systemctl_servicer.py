from .systemctl_service import systemctl_service


class systemctl_servicer:

    def __init__(self):
        self.services = []

    def parse_units(self, units_output):
        results = []
        lines = [i.split() for i in units_output.stdout.split('\n')]
        for line in lines:
            if len(line) > 0:
                results.append(line)
        del results[0]
        del results[-5:]
        self.services = [systemctl_service(i[0], i[3]) for i in results]
        return results

    def compare(self, systemctl_servicer_compare):
        set1 = set(
            (x.name, x.state) for x in systemctl_servicer_compare.services
        )
        comprared_services = systemctl_servicer()
        comprared_services.services = [
            x for x in self.services if (x.name, x.state) not in set1
        ]
        return comprared_services

    def __str__(self):
        ret = ""
        for i in self.services:
            ret = ret + "\n" + str(i)
        return ret
