
class systemctl_service():
    def __init__(self, name, state):
        self.name = name
        self.state = state

    def __str__(self):
        return(self.name + " " + self.state)
