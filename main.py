import argparse
import yaml
import getpass
from colorama import Fore, Style
from includes.group import group
from includes.update import update
from includes.command_fac import command_fac
from multiprocessing import Pool
import click
import logging

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class parralel_job_data:
    def __init__(self, _command_fac, allowed_to_reboot, check):
        self._command_fac = _command_fac
        self.allowed_to_reboot = allowed_to_reboot
        self.check = check


def pool_thread(_parralel_job_data):
    try:
        update(
            _parralel_job_data._command_fac,
            _parralel_job_data.allowed_to_reboot,
            _parralel_job_data.check
        )
    except TimeoutError:
        print(
            Fore.RED + _parralel_job_data._command_fac.hostname + ": "
            "Initial connection to server timeouted!"
        )
    return _parralel_job_data._command_fac.hostname + " async ended"


def dispatche_group(group, user, sudo_pass="", check=False, port=22):
    if group.type == 'sequential':
        for host in group.hosts:
            _command_fac = command_fac(
                host.hostname, user, port, sudo_pass
            )
            try:
                update(
                    _command_fac,
                    host.allowed_to_reboot,
                    check
                )
            except TimeoutError:
                print(
                    Fore.RED + _command_fac.hostname + ": "
                    "Initial connection to server timeouted!"
                )
    elif group.type == 'parallel':
        i = 0
        while i < len(group.hosts):
            _command_fac = command_fac(
                group.hosts[i].hostname, user, port, sudo_pass
            )
            group.hosts[i].add_command_fac(_command_fac)
            i += 1
        coroutines = [
            parralel_job_data(
                host.command_fac, host.allowed_to_reboot, check
            ) for host in group.hosts
        ]
        p = Pool(len(coroutines))
        p.map(pool_thread, coroutines)


def main():
    parser = argparse.ArgumentParser(
        description='Auto updater'
    )
    parser.add_argument(
        '-i',
        '--inventory',
        default='inventories/example.yml',
        help='Inventory of servers',
    )
    parser.add_argument(
        '-p',
        '--port',
        default=22,
        help='Servers\' port'
    )
    parser.add_argument(
        '-K',
        '--sudo-pass',
        default=False,
        action='store_true',
        help='Sudo password'
    )
    parser.add_argument(
        '-c',
        '--check',
        default=False,
        action='store_true',
        help='Cold run'
    )
    parser.add_argument(
        '-u',
        '--user',
        default="nemo",
        help='Which user should be used'
    )

    args = parser.parse_args()

    sudo_pass = ""
    if args.sudo_pass:
        print(
            Fore.YELLOW +
            "Give me sudo password" +
            Style.RESET_ALL
        )
        sudo_pass = getpass.getpass()

    with open(args.inventory, 'r') as f:
        inventory = yaml.load(f)

    if args.check is True:
        print(
            Fore.YELLOW +
            "Only cold run will be executed"
        )
    else:
        print(
            Fore.RED +
            "Running all the commands, no cold run, no humour & stuff"
        )

    print(
        Fore.GREEN +
        "Parsing followin groups: \n"
    )

    groups = []
    for inventory_group in inventory:
        _group = group(inventory_group)
        print(_group)
        groups.append(_group)
    print(Style.RESET_ALL)

    if click.confirm('Do you want to continue?', default=False) is False:
        return

    for _group in groups:
        print(
            Fore.YELLOW +
            "Running " + str(_group) +
            Style.RESET_ALL
        )
        dispatche_group(
            _group,
            args.user,
            sudo_pass,
            args.check,
            args.port
        )


if __name__ == "__main__":
    try:
        main()
    except (KeyboardInterrupt, click.exceptions.Abort):
        print(
            Fore.YELLOW +
            "Cancelled by user interrupt"
        )
